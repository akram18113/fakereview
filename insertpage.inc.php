<?php

$xml = new DOMDocument();
$xml->load($_SESSION['xmlpath']);

$newAct = 'he';

$root = $xml->firstChild;

$newElement = $xml->createElement('DOC');
$root->appendChild($newElement);

$date = $xml->createElement('DATE');
$newElement->appendChild($date);
$newAct = date("d/m/Y");
$newText = $xml->createTextNode($newAct);
$date->appendChild($newText);

$usern = $xml->createElement('AUTHOR');
$newElement->appendChild($usern);
$newAct = $aurthor;
$newText = $xml->createTextNode($newAct);
$usern->appendChild($newText);
/*
  $useri = $xml->createElement('user_id');
  $newElement->appendChild($useri);
  $newAct = $_SESSION['user_id'];
  $newText = $xml->createTextNode($newAct);
  $useri->appendChild($newText);
 */
$txt = $xml->createElement('TEXT');
$newElement->appendChild($txt);
$newAct = $text;
$newText = $xml->createTextNode($newAct);
$txt->appendChild($newText);

$fav = $xml->createElement('FAVORITE');
$newElement->appendChild($fav);
$newAct = $favour;
$newText = $xml->createTextNode($newAct);
$fav->appendChild($newText);

$per = $xml->createElement('percentage');
$newElement->appendChild($per);
$newAct = $percentage;
$newText = $xml->createTextNode($newAct);
$per->appendChild($newText);

$per = $xml->createElement('CollectivePercentage');
$newElement->appendChild($per);
$newAct = $percentage2;
$newText = $xml->createTextNode($newAct);
$per->appendChild($newText);

$per = $xml->createElement('Full_collective_percentage');
$newElement->appendChild($per);
$newAct = $percentgeF3;
$newText = $xml->createTextNode($newAct);
$per->appendChild($newText);

$per = $xml->createElement('Simple_percentage');
$newElement->appendChild($per);
$newAct = $percentage3;
$newText = $xml->createTextNode($newAct);
$per->appendChild($newText);

$per = $xml->createElement('Collective_Simple_percentage');
$newElement->appendChild($per);
$newAct = $percentage4;
$newText = $xml->createTextNode($newAct);
$per->appendChild($newText);

/* 	$per = $xml->createElement('Dice_percentage');
  $newElement->appendChild($per);
  $newAct = $percentage5;
  $newText = $xml->createTextNode($newAct);
  $per->appendChild($newText);

  $per = $xml->createElement('Collective_Dice_percentage');
  $newElement->appendChild($per);
  $newAct = $percentage6;
  $newText = $xml->createTextNode($newAct);
  $per->appendChild($newText); */

$xml->save($_SESSION['xmlpath']);


require_once 'phpexcel\Classes\PHPExcel\IOFactory.php';
$inputFileName = 'insertpage.xlsx';

/** Load $inputFileName to a PHPExcel Object * */
$objPHPExcel = PHPExcel_IOFactory::load($inputFileName);
$objWorksheet = $objPHPExcel->getActiveSheet();
$highestRow = $objWorksheet->getHighestRow();
$highestRow = $highestRow + 1;
// Set cell A1 with a string value
$objPHPExcel->getActiveSheet()->setCellValue('A' . $highestRow, $aurthor);
// Set cell A1 with a string value
$objPHPExcel->getActiveSheet()->setCellValue('B' . $highestRow, $ProName); //Product
// Set cell A1 with a string value
$objPHPExcel->getActiveSheet()->setCellValue('C' . $highestRow, $dat); //Date
// Set cell A2 with a numeric value
$objPHPExcel->getActiveSheet()->setCellValue('D' . $highestRow, $percentage);

// Set cell A3 with a boolean value
$objPHPExcel->getActiveSheet()->setCellValue('E' . $highestRow, $percentage2);

// Set cell A3 with a boolean value
$objPHPExcel->getActiveSheet()->setCellValue('F' . $highestRow, $percentgeF3);
// Set cell A3 with a boolean value
$objPHPExcel->getActiveSheet()->setCellValue('G' . $highestRow, $percentage3);
// Set cell A3 with a boolean value
$objPHPExcel->getActiveSheet()->setCellValue('H' . $highestRow, $percentage4);



$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "Excel2007");
$objWriter->save("insertpage.xlsx");
$similarity = $percentage+$percentage2+$percentgeF3+$percentage3+$percentage4;
$similarity = $similarity/8;
if($similarity > 1){
    $similarity = $similarity-1;
    
}
$similarity = $similarity*100;
$thershold = $percentage*2+$percentage2*1+$percentgeF3*1+$percentage3*2+$percentage4*2;
$thershold = $thershold/11;
if($thershold > 1){
    $thershold = $thershold - 1;
}
$thershold = $thershold * 100;
header('location:commentresult.php?aurthor='.$aurthor.'&text='.$text.'&similarity='.$similarity.'&thershold='.$thershold.'');