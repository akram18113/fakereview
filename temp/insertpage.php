<?php
    $xml = new DOMDocument();
    $xml->load('new.xml');

    $newAct = 'he';

    $root = $xml->firstChild;

    $newElement = $xml->createElement('DOC');
    $root->appendChild($newElement);
	
	$usern = $xml->createElement('username');
	$newElement->appendChild($usern);
    $newText = $xml->createTextNode($newAct);
    $usern->appendChild($newText);
	
	$useri = $xml->createElement('userid');
	$newElement->appendChild($useri);
    $newText = $xml->createTextNode($newAct);
    $useri->appendChild($newText);
	
	$txt = $xml->createElement('comment');
	$newElement->appendChild($txt);
    $newText = $xml->createTextNode($newAct);
    $txt->appendChild($newText);
	
	$fav = $xml->createElement('favorite');
	$newElement->appendChild($fav);
    $newText = $xml->createTextNode($newAct);
    $fav->appendChild($newText);
	
	$per = $xml->createElement('percentage');
	$newElement->appendChild($per);
    $newText = $xml->createTextNode($newAct);
    $per->appendChild($newText);
	
	$date = $xml->createElement('Date');
	$newElement->appendChild($date);
    $newText = $xml->createTextNode($newAct);
    $date->appendChild($newText);
	
    $xml->save('new.xml');
?>